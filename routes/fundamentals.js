const express = require('express');
const router = express.Router();

require('dotenv').config()
const variableData = process.env.variableData || 'Fundamentals'

router.get('/', function (req, res, next) {
    res.send({
        name: 'Fundamentals',
        server: 'express',
        variableData: variableData
    });
});
//1. Sum two numbers
function sumTwoNumbers(a, b) {
    return a + b;
}
router.get('/sumTwoNumbers', function (req, res, next) {
    res.send({
        name: 'sumTwoNumbers',
        sum: sumTwoNumbers(1, 2)
    });
});

//2. Comparsion operators, stric equality
function comparsionOperators(a, b) {
    return a === b && typeof a === typeof b;
}
router.get('/comparsionOperators', function (req, res, next) {
    res.send({
        name: 'comparsionOperators',
        comparsionOperators: comparsionOperators(a, b)
    });
});

//3. Get type of value
function typeOfValue(a) {
    return typeof a;
}
router.get('/typeOfValue', function (req, res, next) {
    res.send({
        name: 'typeOfValue',
        typeOfValue: typeOfValue(a)
    });
});

//4. Get nth character of string

//5. Remove first n characters of string
function removeFirstN(a) {
    return a.slice(3);
}
router.get('/removeFisrtN', function (req, res, next) {
    res.send({
        name: 'removeFirstN',
        removeFirstN: removeFirstN(a)
    });
});


//6. Get last n characters of string
function getLastN(a) {
    return str.slice(-3);
}
router.get('/getLastN', function (req, res, next) {
    res.send({
        name: 'getLastN',
        getLastN: getLastN(a)
    });
});

//7. Get first n characters of string
function getFirstN(a) {
    return a.slice(0, 3);
}
router.get('/getFirstN', function (req, res, next) {
    res.send({
        name: 'getFirstN',
        getFirstN: getFirstN(a)
    });
});

//8. Find the position of one string in another
function findThePosition(a) {
    return a.indexOf('is');
}
router.get('/findThePosition', function (req, res, next) {
    res.send({
        name: 'findThePosition',
        findThePosition: findThePosition(a)
    });
});

//9. Exact first half of string
function exactFirstHalf(a) {
    return a.slice(0, a.length / 2);
}
router.get('/exactFirstHalf', function (req, res, next) {
    res.send({
        name: 'exactFirstHalf',
        exactFirstHalf: exactFirstHalf(a)
    });
});

//10. Remove last n characters of string
function removeLastN(a) {
    return a.slice(0, -3);
}
router.get('/removeLastN', function (req, res, next) {
    res.send({
        name: 'removeLastN',
        removeLastN: removeLastN(a)
    });
});

//11. Return the percentage of a number
function returnThePercentage(a, b) {
    return (b / 100) * a;
}
router.get('/returnThePercentage', function (req, res, next) {
    res.send({
        name: 'returnThePercentage',
        returnThePercentage: returnThePercentage(a)
    });
});

//12. Basic JavaScript math operators

//13. Check whether a string contains another string and concatenate

//14. Check if a number is even
function checkNumberEven(a) {
    return a % 2 === 0;
}
router.get('/checkNumberEven', function (req, res, next) {
    res.send({
        name: 'checkNumberEven',
        checkNumberEven: checkNumberEven(a)
    });
});

//15. How many times dores a characters occur?
function countCharacter(a, b) {
    return b.split(a).length - 1;
}
router.get('/countCharacter', function (req, res, next) {
    res.send({
        name: 'contCharacter',
        countCharacter: countCharacter(a)
    });
});


//16. Check if a number is a whole number
function checkNumberWhole(a) {
    return a % 1 === 0;
}
router.get('/checkNumberWhole', function (req, res, next) {
    res.send({
        name: 'checkNumberWhole',
        checkNumberWhole: checkNumberWhole(a)
    });
});

//17. Multiplication, division, and comparsion operators
function multiplicationAndComparsion(a, b) {
    return a < b ? a / b : a * b;
}
router.get('/multiplicationAndComparsion', function (req, res, next) {
    res.send({
        name: 'multiplicationAndComparsion',
        multiplicationAndComparsion: multiplicationAndComparsion(a)
    });
});

//18. Round a number to 2 decimal places
function roundNumber2DP(a) {
    return Number(a.toFixed(2));
}
router.get('/roundNumber2DP', function (req, res, next) {
    res.send({
        name: 'roundNumber2DP',
        roundNumber2DP: roundNumber2DP(a)
    });
});

//19. Split a number into its digrits









module.exports = router;
